bfs (3.3.1-2) unstable; urgency=medium

  * Apply patch from upstream to fix FTBFS on riscv64. (Closes: #1072933)

 -- Chris Lamb <lamby@debian.org>  Tue, 11 Jun 2024 10:49:08 +0100

bfs (3.3.1-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Tue, 04 Jun 2024 08:26:30 +0100

bfs (3.2-1) unstable; urgency=medium

  * New upstream release.
  * Don't call upstream's (new) configure file.
  * Update renamed Lintian tag.

 -- Chris Lamb <lamby@debian.org>  Wed, 08 May 2024 14:58:05 +0100

bfs (3.1.3-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Sat, 09 Mar 2024 09:34:31 +0000

bfs (3.1.2-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Sun, 03 Mar 2024 15:51:16 +0000

bfs (3.1.1-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Sat, 24 Feb 2024 10:28:49 +0000

bfs (3.1-1) unstable; urgency=medium

  * New upstream release.
  * Add liburing-dev to build-depends.
  * Update debian/docs.

 -- Chris Lamb <lamby@debian.org>  Sun, 11 Feb 2024 13:05:52 -0800

bfs (3.0.4-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Sat, 21 Oct 2023 09:13:57 +0100

bfs (3.0.2-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Sat, 09 Sep 2023 09:40:05 -0700

bfs (3.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.6.2.

 -- Chris Lamb <lamby@debian.org>  Sat, 15 Jul 2023 16:54:05 +0100

bfs (2.6.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.6.1.

 -- Chris Lamb <lamby@debian.org>  Sat, 09 Jul 2022 08:23:39 +0100

bfs (2.6-1) unstable; urgency=medium

  * New upstream release.
  - Update name of RELEASES.md.
  - Update location of bfs.1 manpage.
  * Install more documentation shipped by upstream.

 -- Chris Lamb <lamby@debian.org>  Sat, 28 May 2022 06:53:43 +0100

bfs (2.5-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Sun, 03 Apr 2022 09:42:46 +0100

bfs (2.4.1-1) unstable; urgency=medium

  * New upstream release.
    - Add libonig-dev to Build-Depends.
  * Update debian/copyright.
  * Update debian/watch format.

 -- Chris Lamb <lamby@debian.org>  Sat, 26 Feb 2022 07:11:24 +0000

bfs (2.3.1-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Sat, 22 Jan 2022 08:00:22 -0800

bfs (2.3-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.6.0.

 -- Chris Lamb <lamby@debian.org>  Wed, 01 Dec 2021 08:50:04 -0800

bfs (2.2.1-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Wed, 09 Jun 2021 08:03:39 +0100

bfs (2.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.5.1.

 -- Chris Lamb <lamby@debian.org>  Wed, 10 Mar 2021 08:41:04 +0000

bfs (2.1-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Sat, 14 Nov 2020 11:30:05 +0000

bfs (2.0-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Wed, 21 Oct 2020 12:28:27 +0100

bfs (1.7-1) unstable; urgency=medium

  * New upstream release.
  * Move to debhelper-compat level 13.

 -- Chris Lamb <lamby@debian.org>  Wed, 06 May 2020 16:30:52 +0100

bfs (1.6-4) unstable; urgency=medium

  * Correct reference to DEB_HOST_ARCH_OS to fix FTBFS on kfreebsd-*.

 -- Chris Lamb <lamby@debian.org>  Sun, 08 Mar 2020 13:05:16 -0700

bfs (1.6-3) unstable; urgency=medium

  * Drop extra "D" when disabling extended system attributes on kfreebsd-*.
    Thanks, Tavian.
  * Add missing /usr/share/dpkg/architecture.mk include to ensure population of
    DEB_HOST_ARCH.

 -- Chris Lamb <lamby@debian.org>  Fri, 06 Mar 2020 08:55:44 -0800

bfs (1.6-2) unstable; urgency=medium

  * Fix FTBFS on kFreeBSD.

 -- Chris Lamb <lamby@debian.org>  Wed, 04 Mar 2020 10:05:37 -0800

bfs (1.6-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.5.0.

 -- Chris Lamb <lamby@debian.org>  Sat, 29 Feb 2020 09:28:52 -0800

bfs (1.5.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.4.1.

 -- Chris Lamb <lamby@debian.org>  Tue, 14 Jan 2020 10:58:43 +0000

bfs (1.5.1-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Thu, 19 Sep 2019 10:45:32 +0200

bfs (1.5-3) unstable; urgency=medium

  * Sourceful upload to unstable to ensure migration to testing.
  * Bump Standards-Version to 4.4.0.
  * Don't build release tags in gitlab-ci.yml.

 -- Chris Lamb <lamby@debian.org>  Sat, 20 Jul 2019 17:12:34 -0300

bfs (1.5-2) unstable; urgency=medium

  * Add missing "acl" to Build-Depends.

 -- Chris Lamb <lamby@debian.org>  Fri, 05 Jul 2019 14:24:06 -0300

bfs (1.5-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Thu, 04 Jul 2019 10:08:24 -0300

bfs (1.4.1-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Wed, 22 May 2019 10:06:11 +0100

bfs (1.4-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Mon, 22 Apr 2019 17:51:36 +0100

bfs (1.3.3-1) unstable; urgency=medium

  * New upstream release.
  * Use upstream's "check" target over manually calling the "tests.sh" script
    to ensure the "mksock" binary is built for the testsuite.

 -- Chris Lamb <lamby@debian.org>  Wed, 13 Feb 2019 14:35:39 +0100

bfs (1.3.2-2) unstable; urgency=medium

  * Only require libacl1-dev and libcap-dev on Linux. Thanks to Tavian Barnes.
    (Closes: #920288)

 -- Chris Lamb <lamby@debian.org>  Thu, 24 Jan 2019 09:07:43 +0100

bfs (1.3.2-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Mon, 14 Jan 2019 22:57:42 +0000

bfs (1.3.1-1) unstable; urgency=medium

  * New upstream release.
    - Add libacl1-dev & libcap-dev to Build-Depends.
  * Move to debhelper-compat virtual package.
  * Bump debhelper compatibility level to 12.
  * Bump Standards-Version to 4.3.0.

 -- Chris Lamb <lamby@debian.org>  Sat, 05 Jan 2019 22:21:30 +0100

bfs (1.2.4-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.2.1.

 -- Chris Lamb <lamby@debian.org>  Mon, 01 Oct 2018 15:17:09 +0100

bfs (1.2.3-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Sat, 21 Jul 2018 10:22:41 +0800

bfs (1.2.2-2) unstable; urgency=medium

  * Drop dropping the fstype tests; this should have been fixed upstream:
    https://github.com/tavianator/bfs/commit/85caf29cfd2f40ef10ed76d425bb89a5f8f45a1a.
  * Bump Standards-Version to 4.1.5

 -- Chris Lamb <lamby@debian.org>  Fri, 20 Jul 2018 13:16:24 +0800

bfs (1.2.2-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Mon, 25 Jun 2018 08:33:50 +0100

bfs (1.2.1-2) unstable; urgency=medium

  * Update Vcs-* to point to salsa.debian.org.
  * Bump Standards-Version to 4.1.4.

 -- Chris Lamb <lamby@debian.org>  Thu, 07 Jun 2018 22:51:27 +0100

bfs (1.2.1-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Mon, 12 Feb 2018 10:12:50 +0000

bfs (1.2-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * debian/copyright: Use https for format URI.
  * Move to debhelper compat level 11.
  * Bump Standards-Version to 4.1.3.

 -- Chris Lamb <lamby@debian.org>  Mon, 22 Jan 2018 13:27:26 +1100

bfs (1.1.4-2) unstable; urgency=medium

  * Use upstream's manpage.
  * wrap-and-sort -sa.

 -- Chris Lamb <lamby@debian.org>  Sat, 11 Nov 2017 15:43:00 +0000

bfs (1.1.4-1) unstable; urgency=medium

  * New upstream release.
  * Replace "Priority: extra" with "Priority: optional".

 -- Chris Lamb <lamby@debian.org>  Sat, 11 Nov 2017 10:13:27 +0000

bfs (1.1.3-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.1.1.

 -- Chris Lamb <lamby@debian.org>  Tue, 10 Oct 2017 22:00:43 +0100

bfs (1.1.2-1) unstable; urgency=medium

  * New upstream release.
  * Drop DPKG_EXPORT_BUILDFLAGS and /usr/share/dpkg/buildflags.mk include;
    they are not necessary with debhelper 10.

 -- Chris Lamb <lamby@debian.org>  Tue, 12 Sep 2017 10:53:12 +0100

bfs (1.1.1-1) unstable; urgency=medium

  * New upstream release.
  * Move autopkgtests to separate files.
  * Tidy debian/tests/control.
  * Refresh and renumber patches, sorting using Pq-Topic.

 -- Chris Lamb <lamby@debian.org>  Mon, 14 Aug 2017 08:41:02 -0700

bfs (1.1-1) unstable; urgency=medium

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Fri, 28 Jul 2017 08:42:16 +0100

bfs (1.0.2-6) unstable; urgency=medium

  * Install RELEASES.md, not README.md, as the upstream changelog. Thanks to
    Tavian Barnes for spotting this.

 -- Chris Lamb <lamby@debian.org>  Fri, 21 Jul 2017 20:39:19 +0100

bfs (1.0.2-5) unstable; urgency=medium

  * Don't use the upstream "release" Makefile target as it overrides CFLAGS.
    Thanks to Tavian Barnes.
  * Install upstream's README.md as the upstream changelog.

 -- Chris Lamb <lamby@debian.org>  Fri, 21 Jul 2017 20:02:06 +0100

bfs (1.0.2-4) unstable; urgency=medium

  * Set hardening=+all for bindnow, etc.

 -- Chris Lamb <lamby@debian.org>  Fri, 21 Jul 2017 09:46:28 +0100

bfs (1.0.2-3) unstable; urgency=medium

  * Actually include the manpage in the package.

 -- Chris Lamb <lamby@debian.org>  Sat, 15 Jul 2017 22:40:09 +0100

bfs (1.0.2-2) unstable; urgency=medium

  * Use help2man to generate a manpage.

 -- Chris Lamb <lamby@debian.org>  Fri, 14 Jul 2017 14:50:29 +0100

bfs (1.0.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.0.0.
  * Add a basic smoke autopkgtest.
  * Update 0002-Drop-fstype-tests-as-they-rely-on-etc-mtab-being-ava.patch.

 -- Chris Lamb <lamby@debian.org>  Mon, 26 Jun 2017 09:31:01 +0100

bfs (1.0.1-1) unstable; urgency=medium

  * New upstream release.
  * Correct debian/watch file. Thanks to Tavian Barnes.
  * Refresh patches:
    - 0003-Fix-FTBFS-on-hurd-i386.-Closes-861569.patch dropped upstream

 -- Chris Lamb <lamby@debian.org>  Tue, 23 May 2017 19:11:06 +0100

bfs (1.0-4) unstable; urgency=medium

  * Drop Drop-printf_times-tests-as-they-fail-under-eg.-Docke.patch as it now
    appears to work.

 -- Chris Lamb <lamby@debian.org>  Tue, 02 May 2017 08:59:55 +0100

bfs (1.0-3) unstable; urgency=medium

  * Fix FTBFS on hurd-i386. (Closes: #861569)

 -- Chris Lamb <lamby@debian.org>  Mon, 01 May 2017 08:34:42 +0100

bfs (1.0-2) unstable; urgency=medium

  * Drop fstype tests as they rely on /etc/mtab being available.
    (Closes: #861471)

 -- Chris Lamb <lamby@debian.org>  Sat, 29 Apr 2017 14:00:35 +0100

bfs (1.0-1) unstable; urgency=medium

  * Initial release. (Closes: #861227)

 -- Chris Lamb <lamby@debian.org>  Wed, 12 Apr 2017 21:38:14 +0100
